var Violation = require('../models/violation');

exports.createViolation = function (req, res) {

    var violation = new Violation();
    violation.blockedUri = req.body['csp-report']['blocked-uri'];
    violation.documentUri = req.body['csp-report']['document-uri'];
    violation.violatedDirective = req.body['csp-report']['violated-directive'];
    violation.policy = req.body['csp-report']['original-policy'];
    violation.isEnforced = req.body['csp-report']['disposition'] == 'enforce';
    violation.date = new Date();

    violation.save(function (err) {
        if (!err) {
            res.json('Violation created!');
        }
        else {
            res.json('error occured');
        }
    });
    
    res.json("logged!");
};

